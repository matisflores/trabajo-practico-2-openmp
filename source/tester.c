#include <stdio.h>
#include <stdlib.h>
#define RUNS 30

int main() {
	int i;

	for (i = 0; i < RUNS; i++) {
		system("./doppler");
	}

	printf("%i ejecuciones en serie\n", RUNS);

	for (i = 0; i < RUNS; i++) {
		system("./doppler_parallel");
	}

	printf("%i ejecuciones en paralelo\n", RUNS);

	return 0;
}
