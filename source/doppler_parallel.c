/*
 * \file doppler_parallel.c
 * \brief Procesador Doppler.
 * \author Matias S. Flores
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <omp.h>

#define PULSES 72
#define GATES 500

long bytes2short(char *buffer, long position, unsigned short *result) {
	unsigned short num = buffer[position] | buffer[position + 1] << 8;
	*result = num;
	return 2;
}

long bytes2float(char *buffer, long position, float *result) {
	float num;
#ifdef LITTLE_ENDIAN
	*((unsigned char *) &num + 0) = buffer[position];
	*((unsigned char *) &num + 1) = buffer[position + 1];
	*((unsigned char *) &num + 2) = buffer[position + 2];
	*((unsigned char *) &num + 3) = buffer[position + 3];
#else
	*((unsigned char *) &num + 3) = buffer[position];
	*((unsigned char *) &num + 2) = buffer[position + 1];
	*((unsigned char *) &num + 1) = buffer[position + 2];
	*((unsigned char *) &num + 0) = buffer[position + 3];
#endif
	*result = num;
	return 4;
}

int main(int argc, char *argv[]) {
	FILE *file, *o;
	unsigned short samples;
	int pulse, spg, t_spg, overflow, gate, full, pulses;
	float i, q, v[PULSES][GATES], h[PULSES][GATES], rv[GATES], rh[GATES];
	clock_t ini, fin;
	double time;
	char *buffer;
	long length, readed, readed_v, readed_h;

	ini = clock();
	file = fopen("pulsos.iq", "rb");
	if (!file) {
		perror("Error al abrir el archivo\n");
		return 1;
	}
	fseek(file, 0, SEEK_END);
	length = ftell(file);
	rewind(file);
	buffer = (char*) malloc(sizeof(char) * length);
	if (buffer == NULL) {
		perror("Error en memory alloc\n");
		fclose(file);
		return 2;
	}
	readed = fread(buffer, 1, length, file);
	if (readed != length) {
		perror("Error al leeer archivo\n");
		fclose(file);
		free(buffer);
		return 3;
	}
	fclose(file);

	pulse = 0;
	readed = 0;
	while (readed < length) {
		readed += bytes2short(buffer, readed, &samples);
		spg = samples / 500;
		overflow = samples - spg * 500;
		memset(v[pulse], 0, sizeof(float) * GATES);
		memset(h[pulse], 0, sizeof(float) * GATES);

#pragma omp parallel for private(gate,i,q,t_spg,readed_v,readed_h,full) schedule(auto)
		for (gate = 0; gate < GATES; gate++) {
			t_spg = (gate < overflow ? spg + 1 : spg);
			readed_v = gate * t_spg * sizeof(float) * 2 + readed;
			readed_h = samples * sizeof(float) * 2 + readed_v;
			for (full = 0; full < t_spg; full++) {
				readed_v += bytes2float(buffer, readed_v, &i);
				readed_v += bytes2float(buffer, readed_v, &q);
				v[pulse][gate] = v[pulse][gate] + sqrt(i * i + q * q);
				readed_h += bytes2float(buffer, readed_h, &i);
				readed_h += bytes2float(buffer, readed_h, &q);
				h[pulse][gate] = h[pulse][gate] + sqrt(i * i + q * q);
			}
			v[pulse][gate] = v[pulse][gate] / t_spg;
			h[pulse][gate] = h[pulse][gate] / t_spg;
		}

		readed = samples * sizeof(float) * 4 + readed;
		pulse++;
	}
	free(buffer);
	pulses = pulse;
	gate = GATES;

	o = fopen("doppler_parallel.iq", "wb");
	if (!o) {
		perror("No se pudo crear el archivo");
		return 4;
	}

	memset(rv, 0, sizeof(float) * GATES);
	memset(rh, 0, sizeof(float) * GATES);
	for (gate = 0; gate < GATES; gate++) {
		for (pulse = 0; pulse < pulses - 1; pulse++) {
			rv[gate] += v[pulse][gate] * v[pulse + 1][gate];
			rh[gate] += h[pulse][gate] * h[pulse + 1][gate];
		}
		rv[gate] = rv[gate] / pulses;
		rh[gate] = rh[gate] / pulses;
	}
	fwrite(&rv, sizeof(float), GATES, o);
	fwrite(&rh, sizeof(float), GATES, o);
	fclose(o);

	fin = clock();
	time = (double) (fin - ini) / CLOCKS_PER_SEC;
	printf("%i pulsos procesados en %.6f milisegundos\n", pulses,
			time * 1000.0);

	return 0;
}
