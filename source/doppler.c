/*
 * \file doppler.c
 * \brief Procesador Doppler.
 * \author Matias S. Flores
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#define PULSES 72
#define GATES 500

int main(int argc, char *argv[]) {
	FILE *file, *o;
	unsigned short samples;
	int pulse, spg, t_spg, overflow, gate, full, pulses;
	float i, q, v[PULSES][GATES], h[PULSES][GATES], rv[GATES], rh[GATES];
	clock_t ini, fin;
	double time;

	ini = clock();
	file = fopen("pulsos.iq", "rb");
	if (!file) {
		printf("Error al abrir el archivo");
		return 1;
	}

	pulse = 0;
	while (!feof(file) && pulse < PULSES) {
		fread(&samples, sizeof(unsigned short), 1, file);
		spg = samples / 500;
		overflow = samples - spg * 500;

		memset(v[pulse], 0, sizeof(float) * GATES);
		memset(h[pulse], 0, sizeof(float) * GATES);

		for (gate = 0; gate < GATES; gate++) {
			t_spg = (gate < overflow ? spg + 1 : spg);
			for (full = 0; full < t_spg; full++) {
				fread(&i, sizeof(float), 1, file);
				fread(&q, sizeof(float), 1, file);
				v[pulse][gate] = v[pulse][gate] + sqrt(i * i + q * q);
			}
			v[pulse][gate] = v[pulse][gate] / t_spg;
		}

		for (gate = 0; gate < GATES; gate++) {
			t_spg = (gate < overflow ? spg + 1 : spg);
			for (full = 0; full < t_spg; full++) {
				fread(&i, sizeof(float), 1, file);
				fread(&q, sizeof(float), 1, file);
				h[pulse][gate] = h[pulse][gate] + sqrt(i * i + q * q);
			}
			h[pulse][gate] = h[pulse][gate] / t_spg;
		}
		pulse++;
	}
	fclose(file);
	pulses = pulse;

	o = fopen("doppler.iq", "wb");
	if (!o) {
		perror("No se pudo crear el archivo");
		exit(1);
	}

	memset(rv, 0, sizeof(float) * GATES);
	memset(rh, 0, sizeof(float) * GATES);
	for (gate = 0; gate < GATES; gate++) {
		for (pulse = 0; pulse < pulses - 1; pulse++) {
			rv[gate] += v[pulse][gate] * v[pulse + 1][gate];
			rh[gate] += h[pulse][gate] * h[pulse + 1][gate];
		}
		rv[gate] = rv[gate] / pulses;
		rh[gate] = rh[gate] / pulses;
	}
	fwrite(&rv, sizeof(float), GATES, o);
	fwrite(&rh, sizeof(float), GATES, o);
	fclose(o);

	fin = clock();
	time = (double) (fin - ini) / CLOCKS_PER_SEC;
	printf("%i pulsos procesados en %.6f milisegundos\n", pulses,
			time * 1000.0);

	return 0;
}
